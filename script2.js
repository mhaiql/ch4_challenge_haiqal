//FUNCTION RANDOM//
function getPilihanComp() {
    const comp = Math.random();
    if(comp < 0.34) return "batu"
    if(comp >= 0.34 && comp < 0.67) return "gunting";
    return "kertas"; 
}

//FUNCTION KALAH MENANG SERI
function getHasil(comp, pemain) {
    if(pemain == comp) return "seri";
    if(pemain == "batu") return comp == "gunting" ? "Menang" : "Kalah";
    if(pemain == "gunting") return comp == "batu" ? "Kalah" : "Menang";
    if(pemain == "kertas") return comp == "gunting" ? "Kalah" : "Menang";
}


//FUNCTION UNTUK PILIHAN PLAYER
const pilihBatu = document.querySelector(".batu");
pilihBatu.addEventListener("click", function() {
    const pilihanComputer = getPilihanComp();
    const pilihanPemain = pilihBatu.className;
    const hasil = getHasil(pilihanComputer, pilihanPemain);

    const gambarComputer = document.querySelector(".batu-com");
    gambarComputer.setAttribute("src", "assets/" + pilihanComputer + ".png");

    const info = document.querySelector(".info");
    info.innerHTML = hasil;
});

const pilihGunting = document.querySelector(".gunting");
pilihGunting.addEventListener("click", function() {
    const pilihanComputer = getPilihanComp();
    const pilihanPemain = pilihGunting.className;
    const hasil = getHasil(pilihanComputer, pilihanPemain);

    const gambarComputer = document.querySelector(".gunting-com");
    gambarComputer.setAttribute("src", "assets/" + pilihanComputer + ".png");

    const info = document.querySelector(".info");
    info.innerHTML = hasil;
});

const pilihKertas = document.querySelector(".kertas");
pilihKertas.addEventListener("click", function() {
    const pilihanComputer = getPilihanComp();
    const pilihanPemain = pilihKertas.className;
    const hasil = getHasil(pilihanComputer, pilihanPemain);

    const gambarComputer = document.querySelector(".kertas-com");
    gambarComputer.setAttribute("src", "assets/" + pilihanComputer + ".png");

    const info = document.querySelector(".info");
    info.innerHTML = hasil;
});

function reload() {
    location.reload();
}